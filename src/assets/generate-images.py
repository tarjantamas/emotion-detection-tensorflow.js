import json

batch_number = 0
batch_size = 100

def saveToFile(data, filename):
    with open(filename, 'w') as outfile:
        outfile.write(data)

with open("train.csv", "r") as infile:
    batch = []
    for line in infile:
        tokens = line.split(",")
        if tokens[0] == "Emotion":
            continue
        emotion = tokens[0]
        image = tokens[1].strip('"')
        data = {
            "emotion": int(emotion),
            "image": [int(x.strip().strip('"')) for x in image.split(' ')]
        }

        batch.append(data)

        if len(batch) % batch_size == 0:
            saveToFile(json.dumps(batch), './data/' + str(batch_number) + '.json')
            batch = []
            batch_number += 1
            
print("Generated", batch_number + 1, "batches")
