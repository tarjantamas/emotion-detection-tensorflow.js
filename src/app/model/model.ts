import * as tf from '@tensorflow/tfjs';


export function buildCNN() {
  let model = tf.sequential();

  model.add(tf.layers.conv2d({
    inputShape: [48, 48, 1],
    kernelSize: 5,
    filters: 8,
    strides: 1,
    activation: 'relu',
    kernelInitializer: 'VarianceScaling'
  }));

  model.add(tf.layers.maxPooling2d({
    poolSize: [2, 2],
    strides: [2, 2]
  }));

  model.add(tf.layers.conv2d({
    kernelSize: 5,
    filters: 16,
    strides: 1,
    activation: 'relu',
    kernelInitializer: 'VarianceScaling'
  }));

  model.add(tf.layers.maxPooling2d({
    poolSize: [2, 2],
    strides: [2, 2]
  }));

  model.add(tf.layers.conv2d({
    kernelSize: 5,
    filters: 32,
    strides: 1,
    activation: 'relu',
    kernelInitializer: 'VarianceScaling'
  }));

  model.add(tf.layers.maxPooling2d({
    poolSize: [2, 2],
    strides: [2, 2]
  }));

  model.add(tf.layers.flatten());

  model.add(tf.layers.batchNormalization({ axis: 1 }));

  model.add(tf.layers.dense({
    units: 7,
    kernelInitializer: 'VarianceScaling',
    activation: 'softmax'
  }));

  const LEARNING_RATE = 0.01;
  const optimizer = tf.train.adam(LEARNING_RATE);

  model.compile({
    optimizer: optimizer,
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  return model;
}

export function buildDense() {
  let model = tf.sequential();

  model.add(tf.layers.flatten({ inputShape: [48, 48, 1] }));

  model.add(tf.layers.dense({
    units: 7,
    kernelInitializer: 'VarianceScaling',
    activation: 'softmax'
  }));

  const LEARNING_RATE = 0.01;
  const optimizer = tf.train.adam(LEARNING_RATE);

  model.compile({
    optimizer: optimizer,
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  return model;
}

export function experimentalModel() {
  let model = tf.sequential();

  model.add(tf.layers.flatten({ inputShape: [48, 48, 1] }));

  model.add(tf.layers.batchNormalization({ axis: -1 }));

  model.add(tf.layers.dense({
    units: 100,
    kernelInitializer: 'VarianceScaling',
    activation: 'relu'
  }));

  model.add(tf.layers.dense({
    units: 7,
    kernelInitializer: 'VarianceScaling',
    activation: 'relu'
  }));

  model.add(tf.layers.dense({
    units: 7,
    kernelInitializer: 'VarianceScaling',
    activation: 'softmax'
  }));

  const LEARNING_RATE = 0.01;
  const optimizer = tf.train.adam(LEARNING_RATE);

  model.compile({
    optimizer: optimizer,
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  return model;
}