import { Component, OnInit } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { DataService } from './data/data.service';
import { environment } from '../environments/environment';
import { ChartData } from './chart/chart-data.model';
import { buildCNN, buildDense, experimentalModel } from './model/model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  model: tf.Model;

  prediction: any;

  lossChart = new ChartData("Training Loss");

  accuracyChart = new ChartData("Training Accuracy");

  modelName: string;

  jsonFile: any;

  binFile: any;

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.initModel();
  }

  initModel() {
    this.modelName = 'cnn';
    this.model = buildCNN();
  }

  async train() {
    this.lossChart = new ChartData('Training Loss');
    this.accuracyChart = new ChartData('Training Accuracy');
    this.lossChart.initDataGroup('loss');
    this.accuracyChart.initDataGroup("accuracy");

    const BATCH_SIZE = environment.batchSize;
    const TRAIN_BATCHES = 41;
    const TEST_ITERATION_FREQUENCY = 1;
    const EPOCHS = 1000;

    for (let i = 0; i < TRAIN_BATCHES * EPOCHS; i++) {
      let index = i % TRAIN_BATCHES;
      let trainBatch = await this.dataService.getBatch(index).toPromise();
      let validationData;

      // Every few batches test the accuracy of the mode.
      // if (i % TEST_ITERATION_FREQUENCY === 0) {
      //   let testBatch = await this.dataService.getBatch(index).toPromise();
      //   validationData = [
      //     testBatch.xs.reshape([BATCH_SIZE, 48, 48, 1]), testBatch.labels
      //   ];
      // }

      const history = await this.model.fit(
        trainBatch.xs.reshape([BATCH_SIZE, 48, 48, 1]),
        trainBatch.labels,
        {
          batchSize: BATCH_SIZE,
          validationData,
          epochs: 1
        });

      const loss = history.history.loss[0];
      const accuracy = history.history.acc[0];

      if (i % TRAIN_BATCHES === 0) {
        this.lossChart.addData('loss', i, loss);
        this.accuracyChart.addData('accuracy', i, accuracy);
        console.log(loss, accuracy);
      }
    }
  }

  async predict() {
    const BATCH_SIZE = environment.batchSize;
    let batch = await this.dataService.getBatch(0).toPromise();
    batch.labels.print();
    let result: any = this.model.predict(batch.xs.reshape([BATCH_SIZE, 48, 48, 1]));
    result.print();
  }

  save() {
    this.model.save(`downloads://${this.modelName}`);
  }

  onJsonChange(event) {
    this.jsonFile = event.target.files;
    console.log(event);
  }

  onBinChange(event) {
    this.binFile = event.target.files;
  }

  async load() {
    const jsonUpload: any = this.jsonFile;
    const weightsUpload: any = this.binFile;

    this.model = await tf.loadModel(
      tf.io.browserFiles([jsonUpload[0], weightsUpload[0]]));

    const LEARNING_RATE = 0.01;
    const optimizer = tf.train.adam(LEARNING_RATE);
    this.model.compile({
      optimizer: optimizer,
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy'],
    });

    alert("Loaded");
  }


}
