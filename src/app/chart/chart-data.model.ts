export class ChartData {
  view: any[] = [800, 300];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  single: any[];
  multi: any[] = [];
  multiIndex = {};

  autoScale = true;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  showYAxisLabel = true;
  yAxisLabel = 'Value';

  name: string;

  constructor(name: string) {
    this.name = name;
    this.initDataGroup(name);
  }

  reverseNames() {
    for (let group of this.multi) {
      console.log(group);
      let series: any[] = group.series;
      series.sort((a, b) => a.name.localeCompare(b.name));
    }
  }

  initDataGroup(name) {
    let group = {
      name: name,
      series: []
    };
    this.multiIndex[name] = group;
    this.multi.push(group);
  }

  containsGroup(name) {
    return this.multiIndex[name];
  }

  addData(groupName, dataName, dataValue) {
    let group = this.multiIndex[groupName];
    group.series.push({
      name: `${dataName}`,
      value: dataValue
    });

    if (group.series.length > 200) {
      group.series.splice(0, 50);
    }

    this.multi = [...this.multi];
  }
}