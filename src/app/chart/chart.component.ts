import { Component, OnInit, Input } from '@angular/core';
import { ChartData } from './chart-data.model';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  @Input()
  chart: ChartData;

  @Input()
  name: string = "Default";

  constructor() { }

  ngOnInit() {
  }

}
