import { Tensor } from "@tensorflow/tfjs";

export class Batch {

    constructor(
        public xs: Tensor,
        public labels: Tensor
    ) { }

}