export class ImageItem {

    constructor(
        public emotion: number,
        public image: number[]
    ) { }

}