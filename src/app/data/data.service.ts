import {map} from 'rxjs/internal/operators';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Batch } from './batch.model';
import { ImageItem } from './image-item.model';
import * as tf from '@tensorflow/tfjs';
import { environment } from '../../environments/environment';

const IMAGE_SIZE = environment.imageSize;

const NUM_CLASSES = environment.classCount;

// (0 = Angry, 1 = Disgust, 2 = Fear, 3 = Happy, 4 = Sad, 5 = Surprise, 6 = Neutral)

const LABELS = {
  0: [1, 0, 0, 0, 0, 0, 0],
  1: [0, 1, 0, 0, 0, 0, 0],
  2: [0, 0, 1, 0, 0, 0, 0],
  3: [0, 0, 0, 1, 0, 0, 0],
  4: [0, 0, 0, 0, 1, 0, 0],
  5: [0, 0, 0, 0, 0, 1, 0],
  6: [0, 0, 0, 0, 0, 0, 1]
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  private url = 'http://localhost:4200/assets/data';

  private currentBatchIndex = 0;

  private currentTestBatchIndex = 333;

  constructor(
    private http: Http
  ) { }

  private getData(index): Observable<ImageItem[]> {
    return this.http.get(`${this.url}/${index}.json`)
      .pipe(map(response => response.json()))
  }

  getNextTrainBatch(): Observable<Batch> {
    let index = this.currentBatchIndex;
    this.currentBatchIndex++;
    return this.getBatch(index);
  }

  getNextTestBatch(): Observable<Batch> {
    let index = this.currentTestBatchIndex;
    this.currentTestBatchIndex++;
    return this.getBatch(index);
  }
  
  getBatch(index): Observable<Batch> {
    return this.getData(index)
      .pipe(map(data => {
        let labels = [];
        let xs = [];

        for (let item of data) {
          xs.push(...item.image);
          labels.push(LABELS[item.emotion]);
        }

        return new Batch(tf.tensor2d(xs, [data.length, IMAGE_SIZE]), tf.tensor2d(labels, [data.length, NUM_CLASSES]));
      }));
  }
}
